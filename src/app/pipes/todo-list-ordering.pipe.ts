import { Pipe, PipeTransform } from '@angular/core';

import { Todo } from '../models/todo';
import { SettingsService } from '../services/settings.service';

@Pipe({
  name: 'todoListOrdering'
})
export class TodoListOrderingPipe implements PipeTransform {

  constructor(
    private settingsService: SettingsService
  ) { }

  transform(items: Todo[], value: string): Todo[] {
    if (!items) {
      return [];
    }
    if (!this.settingsService.listSortEnabled) {
      return items;
    }
    return items.sort((t1, t2) => this.orderByIdAndStatus(t1, t2));
  }

  private orderByIdAndStatus(t1: Todo, t2: Todo): number {
    if (t1.done && !t2.done) {
      return 1;
    }
    if (t2.done && !t1.done) {
      return -1;
    }
    if ((t1.done && t2.done || !t1.done && !t2.done) && t1.label.toLowerCase() > t2.label.toLowerCase()) {
      return 1;
    }
    if ((t1.done && t2.done || !t1.done && !t2.done) && t1.label.toLowerCase() < t2.label.toLowerCase()) {
      return -1;
    }
  }

}

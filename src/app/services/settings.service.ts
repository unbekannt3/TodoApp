import { Injectable } from '@angular/core';

import { MessageBoxService } from './message-box.service';
import { TodoCategoryService } from './todo-category.service';
import { TodoDataService } from './todo-data.service';
import { TodoPriorityService } from './todo-priority.service';

declare var swal: any;

@Injectable()
export class SettingsService {

  private version = '1.0.3';
  private rotateActive = false;

  constructor(
    private todoDataService: TodoDataService,
    private todoCategoryService: TodoCategoryService,
    private todoPriorityService: TodoPriorityService,
    private messageBoxService: MessageBoxService
  ) { }



  // load example todos
  loadExampleData() {
    // clear current todo array
    if (this.todoDataService.todos.length != null) {
      this.todoDataService.todoList.length = 0;
    }
    // fill with example todos
    JSON.parse(localStorage.getItem('exampleTodos')).forEach(todo => {
      this.todoDataService.add(todo);
    });
    this.messageBoxService.display('Beispieldaten erfolgreich geladen', 'success');

    // update localStorage state
    this.updateLocalStorageArray();
  }

  // delete state button
  toggleDeleteButton() {
    if (localStorage.getItem('delEnabled') === 'false' || null) {
      localStorage.setItem('delEnabled', 'true');
    } else {
      localStorage.setItem('delEnabled', 'false');
    }
  }

  // searchfield state button
  toggleSearchField() {
    if (localStorage.getItem('searchEnabled') === 'false' || null) {
      localStorage.setItem('searchEnabled', 'true');
    } else {
      localStorage.setItem('searchEnabled', 'false');
    }
  }

  // todo ordering state button
  toggleTodoOrdering() {
    if (localStorage.getItem('todoOrdering') === 'false' || null) {
      localStorage.setItem('todoOrdering', 'true');
    } else {
      localStorage.setItem('todoOrdering', 'false');
    }
  }

  // Alle Todos löschen
  deleteAll(alert?: boolean) {
    if (alert === null) {
      alert = true;
    }
    this.todoDataService.clearArray();
  }

  // update localStorage
  updateLocalStorageArray() {
    localStorage.setItem('todos', JSON.stringify(this.todoDataService.todos));
    console.log('[DEBUG] [localStorage] updated todos');
  }

  get delEnabled(): boolean {
    if (localStorage.getItem('delEnabled') === 'true') {
      return true;
    } else {
      return false;
    }

  }

  get searchEnabled(): boolean {
    if (localStorage.getItem('searchEnabled') === 'true') {
      return true;
    } else {
      return false;
    }

  }

  get listSortEnabled(): boolean {
    if (localStorage.getItem('todoOrdering') === 'true') {
      return true;
    } else {
      return false;
    }
  }

  get appVersion() {

    return this.version;
  }

}

import { Injectable } from '@angular/core';
import { AlertService } from 'ngx-alerts';

@Injectable()
export class MessageBoxService {

  private boxtext: string;
  private typ: string;
  private show = false;

  constructor(
    private alertService: AlertService
  ) { }

  display(message: string, typ?: string) {
    if (typ === null || '') {
      typ = 'info';
    }
    if (typ === 'success') {
      this.alertService.success(message);
    }
    if (typ === 'error' || typ === 'danger') {
      this.alertService.danger(message);
    }
    if (typ === 'warn' || typ === 'warning') {
      this.alertService.warning(message);
    }
    if (typ === 'info') {
      this.alertService.info(message);
    }
    this.typ = typ;
    return this.boxtext = message;
  }

}

import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { Todo } from '../models/todo';
import { MessageBoxService } from './message-box.service';
import { TodoCategoryService } from './todo-category.service';
import { TodoPriorityService } from './todo-priority.service';

@Injectable()
export class TodoDataService {

  public todos: Todo[] = [];
  private exampleTodos: Todo[] = [];

  constructor(
    public todoCategoryService: TodoCategoryService,
    public messageBoxService: MessageBoxService,
    public todoPriorityService: TodoPriorityService,
    public router: Router
  ) {

    // example data
    this.exampleTodos.push(new Todo(1, 'Staubsaugen', new Date(),
      this.todoCategoryService.categories[0], this.todoPriorityService.priorities[0]));
    this.exampleTodos.push(new Todo(2, 'Rasen mähen', new Date(),
      this.todoCategoryService.categories[1], this.todoPriorityService.priorities[0]));
    this.exampleTodos.push(new Todo(3, 'Spiel kaufen', new Date(2019, 8, 26),
      this.todoCategoryService.categories[2], this.todoPriorityService.priorities[1]));
    this.exampleTodos.push(new Todo(4, 'Abo kündigen', new Date(2017, 12, 14),
      this.todoCategoryService.categories[3], this.todoPriorityService.priorities[2], true));
    this.exampleTodos.push(new Todo(5, 'Vorräte auffüllen', new Date(2018, 12, 22),
      this.todoCategoryService.categories[0], this.todoPriorityService.priorities[2]));
    this.exampleTodos.push(new Todo(6, 'Unnütze Bäume fällen', new Date(2018, 8, 14),
      this.todoCategoryService.categories[1], this.todoPriorityService.priorities[2]));
    this.exampleTodos.push(new Todo(7, 'PC Aufrüsten', new Date(),
      this.todoCategoryService.categories[2], this.todoPriorityService.priorities[0]));
    this.exampleTodos.push(new Todo(8, 'Bank wechseln', new Date(2018, 2, 9),
      this.todoCategoryService.categories[3], this.todoPriorityService.priorities[1], true));
    this.exampleTodos.push(new Todo(9, 'Müll ausleeren', new Date(2017, 9, 8),
      this.todoCategoryService.categories[0], this.todoPriorityService.priorities[1]));

    // create properties in localStorage if not found or overwrite if in different state
    // todos
    if (JSON.parse(localStorage.getItem('todos')) === null) {
      localStorage.setItem('todos', JSON.stringify(this.todos));
    }
    // example todos
    if (localStorage.getItem('exampleTodos') === null || JSON.parse(localStorage.getItem('exampleTodos')) !== this.exampleTodos) {
      localStorage.setItem('exampleTodos', JSON.stringify(this.exampleTodos));
    }
    // delete active
    if (localStorage.getItem('delEnabled') === null) {
      localStorage.setItem('delEnabled', 'true');
    }
    // searchfield active
    if (localStorage.getItem('searchEnabled') === null) {
      localStorage.setItem('searchEnabled', 'true');
    }
    // ordering todos active
    if (localStorage.getItem('todoOrdering') === null) {
      localStorage.setItem('todoOrdering', 'false');
    }

    // parse todos in array from localStorage
    this.todos = JSON.parse(localStorage.getItem('todos'));
  }

  // return todo array as fake property
  get todoList() {
    return this.todos;
  }

  // change todo state to done or undone
  toggle(_todo: Todo) {
    _todo.done = !_todo.done;

    // update localStorage state
    this.updateLocalStorageArray();
  }

  // add todo
  add(_todo: Todo, alert?: boolean) {
    if (alert === null) {
      alert = true;
    }

    if (_todo.label && _todo.category && _todo.priority) {

      this.todos.push(new Todo(this.todos.length + 1, _todo.label, _todo.dueDate, _todo.category, _todo.priority, _todo.done));
      if (alert) {
        this.messageBoxService.display('Todo "' + _todo.label + '" erfolgreich hinzugefügt', 'success');
      }
      _todo.label = '';
      this.router.navigateByUrl('todos');

      // update localStroage state
      this.updateLocalStorageArray();

    } else {
      this.messageBoxService.display('Bitte trage Mindestens einen Titel und die Kategorie ein!', 'danger');
    }
  }

  // delete one todo
  delete(_todo: Todo) {

    this.todos = this.todos.filter(t => t.id !== _todo.id);
    for (let i = 0; i < this.todos.length; i++) {
      this.todos[i].id = i + 1;
    }
    this.messageBoxService.display('Todo "' + _todo.label + '" erfolgreich gelöscht', 'warning');

    // update localStorage state
    this.updateLocalStorageArray();
  }

  // clear todo array
  clearArray(alert?: boolean) {
    if (alert === null) {
      alert = true;
    }

    // clear array
    this.todos.length = 0;
    if (alert) {
      this.messageBoxService.display('Deine Todos wurden beerdigt', 'warning');
    }

    // update localStorage
    this.updateLocalStorageArray();

  }

  // save array in localStorage (update)
  updateLocalStorageArray() {
    localStorage.setItem('todos', JSON.stringify(this.todos));
    console.log('[DEBUG] [localStorage] updated todos');
  }

  // todo array empty check
  leer(): boolean {

    let leer: boolean;
    if (this.todos.length < 1) {
      leer = true;
    } else {
      leer = false;
    }
    return leer;
  }

  // todo array empty check as fake property
  get todosLeer(): boolean {
    if (this.todos.length < 1) {
      return true;
    } else {
      return false;
    }
  }

  // get next array id
  get nextID(): number {
    return this.todos.length++;
  }

}

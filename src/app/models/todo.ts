import { Category } from './category';
import { Priority } from './priority';
export class Todo {

    constructor(
        public id?: number,
        public label?: string,
        public dueDate?: Date,
        public category?: Category,
        public priority?: Priority,
        public done: boolean = false
    ) { }

}
